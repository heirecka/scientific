# Copyright 2010 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'gnucap-0.35.20090611.ebuild' from Gentoo, which is:
#     Copyright 1999-2009 Gentoo Foundation

export_exlib_phases src_prepare src_compile src_install

SNAPSHOTDATE="${PNV##*.}"
MY_PNV="${PN}-${SNAPSHOTDATE:0:4}-${SNAPSHOTDATE:4:2}-${SNAPSHOTDATE:6:2}"

SUMMARY="GNUCap is the GNU Circuit Analysis Package"
DESCRIPTION="
Gnu Circuit Analysis Package is a general purpose circuit simulator. It performs
nonlinear DC and transient analysis, fourier analysis, and AC analysis. Spice
compatible models for the MOSFET (level 1- 7) and diode are included. This project
is not based on Berkeley Spice, but some of the models have been derived from the
Berkeley models. It was formerly known as 'Al's Circuit Simulator'.
"
HOMEPAGE="http://www.${PN}.org"
DOWNLOADS="
    ${HOMEPAGE}/devel/${MY_PNV}.tar.gz
    ${HOMEPAGE}/devel/${MY_PNV}-models-bsim.tar.gz
    ${HOMEPAGE}/devel/${MY_PNV}-models-jspice3-2.5.tar.gz
    ${HOMEPAGE}/devel/${MY_PNV}-models-ngspice17.tar.gz
    ${HOMEPAGE}/devel/${MY_PNV}-models-spice3f5.tar.gz
"

REMOTE_IDS="freecode:${PN}"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/${PN}-man-html/index.html [[ lang = en ]]"

SLOT="0"
LICENCES="GPL-2"
MYOPTIONS="examples"

DEPENDENCIES=""

WORK="${WORKBASE}"/${MY_PNV}

DEFAULT_SRC_COMPILE_PARAMS=( CC="${CC}" CCC="${CXX}" )

gnucap_src_prepare() {
    # No need to install COPYING and INSTALL
    edo sed -e 's: COPYING INSTALL::' \
            -e 's:COPYING history INSTALL:history:' \
            -i doc/Makefile.in

    # No TeX, no docs.
    edo sed -e 's:doc examples:examples:' \
            -i Makefile.in

    if ! option examples ; then
        edo sed -e 's:examples modelgen:modelgen:' \
                -i Makefile.in
    fi

    # Respect flags
    edo sed -e 's:CFLAGS = -O2 -g:CPPFLAGS +=:' \
            -e '/CCFLAGS =/i\CFLAGS += $(CPPFLAGS)' \
            -e 's:CCFLAGS = $(CFLAGS):CXXFLAGS += $(CPPFLAGS):' \
            -e 's:LDFLAGS = :LDFLAGS += :' \
            -e 's:CCFLAGS:CXXFLAGS:' \
            -e "s:../Gnucap:${WORK}/src:" \
            -i models-*/Make2
}

gnucap_src_compile() {
    default

    for PLUGIN_DIR in models-* ; do
        cd "${WORK}/${PLUGIN_DIR}"
        default
    done
}

gnucap_src_install() {
    default

    insopts -m0755
    for PLUGIN_DIR in models-* ; do
        insinto /usr/$(exhost --target)/lib/gnucap/${PLUGIN_DIR}
        cd "${WORK}/${PLUGIN_DIR}"
        for PLUGIN in */*.so ; do
            newins ${PLUGIN} ${PLUGIN##*/}
        done
    done
}

